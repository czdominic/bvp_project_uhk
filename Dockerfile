# Start with a base image containing Java runtime
FROM openjdk:8
ADD target/todolist_be-1.jar todolist_be-1.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar", "todolist_be-1.jar"]
