package cz.uhk.projekt.bvp.todolist_be.repository;

import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.CreateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.DeleteItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.UpdateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.entity.TodoItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TodoItemDAO implements TodoItemInt {

    @PersistenceContext
    EntityManager em;
    private TodoItemDTOOut todoItemDTOOut = new TodoItemDTOOut();

    public List<TodoItem> getAllItems() {
        String query = "select t from TodoItem t where t.readydelete=false";
        return em.createQuery(query).getResultList();
    }

    public TodoItemDTOOut createItem(CreateItemDTOIn createItemDTOIn) {
        TodoItem todoItem = new TodoItem(createItemDTOIn.getDate(), createItemDTOIn.getDescription(), createItemDTOIn.getName(), createItemDTOIn.getDone());
        try {
            em.persist(todoItem);
            todoItemDTOOut.setId(todoItem.getId());
            todoItemDTOOut.setStatusCode(1);
            todoItemDTOOut.setMessage("Item vytvořen.");
        } catch (Exception e) {
            todoItemDTOOut.setStatusCode(-1);
            todoItemDTOOut.setMessage("Chyba serveru.");
        }
        return todoItemDTOOut;
    }

    public TodoItemDTOOut deleteItem(DeleteItemDTOIn deleteItemDTOIn) {
        try {
            TodoItem itemtoDelete = em.find(TodoItem.class, deleteItemDTOIn.getId());
            if (itemtoDelete != null) {
                if (deleteItemHard(deleteItemDTOIn.getId())) {
                    em.remove(em.find(TodoItem.class, deleteItemDTOIn.getId()));
                } else {
                    em.createQuery("UPDATE TodoItem t set t.readydelete=true where t.id=:id").setParameter("id", deleteItemDTOIn.getId()).executeUpdate();
                }
                todoItemDTOOut.setStatusCode(1);
                todoItemDTOOut.setMessage("Item odstraněn.");
            } else {
                todoItemDTOOut.setStatusCode(-1);
                todoItemDTOOut.setMessage("Item nenalezen.");
            }
        } catch (
                Exception e) {
            todoItemDTOOut.setStatusCode(-1);
            todoItemDTOOut.setMessage("Chyba serveru, opakujte akci později.");
        }
        return todoItemDTOOut;
    }

    public TodoItemDTOOut updateItem(UpdateItemDTOIn updatedItem) {
        try {
            if (em.find(TodoItem.class, updatedItem.getId()) != null) {
                em.createQuery("UPDATE TodoItem t SET t.done=:done where t.id=:id")
                        .setParameter("id", updatedItem.getId())
                        .setParameter("done", updatedItem.getDone())
                        .executeUpdate();
                todoItemDTOOut.setStatusCode(1);
                todoItemDTOOut.setMessage("Item upraven.");
            } else {
                todoItemDTOOut.setStatusCode(-1);
                todoItemDTOOut.setMessage("Item nenalezen.");
            }
        } catch (Exception e) {
            todoItemDTOOut.setStatusCode(-1);
            todoItemDTOOut.setMessage("Chyba serveru.");
        }
        return todoItemDTOOut;
    }

    private boolean deleteItemHard(int id) {
        Query query = em.createNativeQuery("SELECT count(*) FROM history h inner join todo_item t where t.id=:id");
        query.setParameter("id", id);
        int count = ((Number) query.getSingleResult()).intValue();
        return count == 0;
    }
}
