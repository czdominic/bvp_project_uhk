package cz.uhk.projekt.bvp.todolist_be.controller;

import cz.uhk.projekt.bvp.todolist_be.controller.path.UrlConstant;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.CreateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.DeleteItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.UpdateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.entity.TodoItem;
import cz.uhk.projekt.bvp.todolist_be.repository.TodoItemInt;
import cz.uhk.projekt.bvp.todolist_be.validation.TodoItemValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(UrlConstant.WISHITEM)
public class TodoItemController {

    TodoItemDTOOut todoItemDTOOut;

    @Autowired
    TodoItemInt todoItemInt;

    @Autowired
    TodoItemValidation todoItemValidation;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TodoItem> getAllItems() {
        return todoItemInt.getAllItems();
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public TodoItemDTOOut createItem(@RequestBody CreateItemDTOIn createItemDTOIn) {

        todoItemDTOOut = todoItemValidation.validateName(createItemDTOIn.getName());
        if (todoItemDTOOut.getStatusCode() == 1) {
            todoItemDTOOut = todoItemValidation.validateDescription(createItemDTOIn.getDescription());
            if (todoItemDTOOut.getStatusCode() == 1) {
                todoItemDTOOut = todoItemValidation.validateDate(createItemDTOIn.getDate());
                if (todoItemDTOOut.getStatusCode() == 1) {
                    todoItemDTOOut = todoItemInt.createItem(createItemDTOIn);
                }
            }
        }
        return todoItemDTOOut;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public TodoItemDTOOut updateItem(@RequestBody UpdateItemDTOIn updateItemDTOin) {
                    todoItemDTOOut = todoItemInt.updateItem(updateItemDTOin);
        return todoItemDTOOut;
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public TodoItemDTOOut deleteItem(@RequestBody DeleteItemDTOIn deleteItemDTOIn) {
        return todoItemInt.deleteItem(deleteItemDTOIn);
    }


}
