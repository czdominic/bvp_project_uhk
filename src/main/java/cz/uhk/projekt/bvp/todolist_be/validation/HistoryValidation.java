package cz.uhk.projekt.bvp.todolist_be.validation;

import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.HistoryDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import org.springframework.stereotype.Service;

@Service
public class HistoryValidation {

    private static final String REG_EX_TEXT = "^[\\p{L}|\\d]+[\\p{L} \\d]{1,}$";
    private static final String REG_EX_DATE = "^\\d{2}/\\d{2}/\\d{4}[ ]{1}\\d{2}:\\d{2}";

    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 30;

    private static final int ACTION_MIN_LENGTH = 3;
    private static final int ACTION_MAX_LENGTH = 30;

    private HistoryDTOOut historyDTOOut = new HistoryDTOOut();


    public HistoryDTOOut validateName(String name) {
        String message = "";
        int statusCode = 1;

        if (name == null || name.replaceAll("\\s", "").isEmpty() || name.trim().length() < NAME_MIN_LENGTH) {
            statusCode = -1;
            message = "Název musí obsahovat alespoň " + NAME_MIN_LENGTH + " znaky.";
        } else {
            if (name.length() > NAME_MAX_LENGTH) {
                statusCode = -1;
                message = "Název může obsahovat maximálně " + NAME_MAX_LENGTH + " znaků.";
            } else {
                if (!name.matches(REG_EX_TEXT)) {
                    statusCode = -1;
                    message = "Název obsahuje nepovolené znaky";
                }
            }
        }
        historyDTOOut.setStatusCode(statusCode);
        historyDTOOut.setMessage(message);
        return historyDTOOut;
    }

    public HistoryDTOOut validateAction(String action) {
        String message = "";
        int statusCode = 1;

        if (action == null || action.replaceAll("\\s", "").isEmpty() || action.trim().length() < ACTION_MIN_LENGTH) {
            statusCode = -1;
            message = "Název musí obsahovat alespoň " + ACTION_MIN_LENGTH + " znaky.";
        } else {
            if (action.length() > ACTION_MAX_LENGTH) {
                statusCode = -1;
                message = "Název může obsahovat maximálně " + ACTION_MAX_LENGTH + " znaků.";
            } else {
                if (!action.matches(REG_EX_TEXT)) {
                    statusCode = -1;
                    message = "Název obsahuje nepovolené znaky";
                }
            }
        }
        historyDTOOut.setStatusCode(statusCode);
        historyDTOOut.setMessage(message);
        return historyDTOOut;
    }

    public HistoryDTOOut validateDate(String date) {
        String message = "";
        int statusCode = 1;

        if (date == null) {
            message = "Je třeba zadat datum.";
            statusCode = -1;
        } else {
            if (!date.matches(REG_EX_DATE)) {
                statusCode = -1;
                message = "Datum je třeba zadat ve správném formátu. dd/mm/yyyy hh:mm";
            }
        }
        historyDTOOut.setStatusCode(statusCode);
        historyDTOOut.setMessage(message);
        return historyDTOOut;
    }

}
