package cz.uhk.projekt.bvp.todolist_be.model.dtoOut;

public class TodoItemDTOOut {
    private int statusCode;
    private String message;
    private int id;

    public TodoItemDTOOut() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
