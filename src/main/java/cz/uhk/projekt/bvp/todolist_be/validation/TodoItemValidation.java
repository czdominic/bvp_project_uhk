package cz.uhk.projekt.bvp.todolist_be.validation;

import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class TodoItemValidation {


    private static final String REG_EX_TEXT = "^[\\p{L}|\\d]+[\\p{L} \\d]{1,}$";
    private static final String REG_EX_DATE = "^\\d{2}/\\d{2}/\\d{4}[ ]{1}\\d{2}:\\d{2}";

    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 50;

    private static final int DESCRIPTION_MAX_LENGTH = 200;

    private TodoItemDTOOut todoItemDTOOut = new TodoItemDTOOut();

    public TodoItemDTOOut validateName(String name) {
        String message = "";
        int statusCode = 1;

        if (name == null || name.replaceAll("\\s", "").isEmpty() || name.trim().length() < NAME_MIN_LENGTH) {
            statusCode = -1;
            message = "Název může obsahovat alespoň " + NAME_MIN_LENGTH + " znaky.";
        } else {
            if (name.length() > NAME_MAX_LENGTH) {
                statusCode = -1;
                message = "Název může obsahovat maximálně " + NAME_MAX_LENGTH + " znaků.";
            } else {
                if (!name.matches(REG_EX_TEXT)) {
                    statusCode = -1;
                    message = "Název obsahuje nepovolené znaky";
                }
            }
        }
        todoItemDTOOut.setStatusCode(statusCode);
        todoItemDTOOut.setMessage(message);
        return todoItemDTOOut;
    }

    public TodoItemDTOOut validateDescription(String description) {
        String message = "";
        int statusCode = 1;
        if (!description.equals("")) {
            if (description.length() > DESCRIPTION_MAX_LENGTH) {
                statusCode = -1;
                message = "Popis může obsahovat maximálně " + DESCRIPTION_MAX_LENGTH + " znaků.";
            } else {
                if (!description.matches(REG_EX_TEXT)) {
                    statusCode = -1;
                    message = "Popis obsahuje nepovolené znaky";
                }
            }
        }
        todoItemDTOOut.setStatusCode(statusCode);
        todoItemDTOOut.setMessage(message);
        return todoItemDTOOut;
    }

    public TodoItemDTOOut validateDate(String date) {
        String message = "";
        int statusCode = 1;

        if (date == null) {
            message = "Je třeba zadat datum.";
            statusCode = -1;
        } else {
            if (!date.matches(REG_EX_DATE)) {
                statusCode = -1;
                message = "Datum je třeba zadat ve správném formátu. dd/mm/yyyy hh:mm";
            }
        }
        todoItemDTOOut.setStatusCode(statusCode);
        todoItemDTOOut.setMessage(message);
        return todoItemDTOOut;
    }

}
