package cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem;


import javax.persistence.Column;

public class CreateItemDTOIn {

    private String name;

    private String description;

    private String date;

    private Boolean done;

    public Boolean getDone() {
        return done;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }
}
