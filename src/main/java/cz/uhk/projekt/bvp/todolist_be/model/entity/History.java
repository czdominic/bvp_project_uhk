package cz.uhk.projekt.bvp.todolist_be.model.entity;

import com.sun.istack.Nullable;

import javax.persistence.*;

@Entity
public class History {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String action;

    @Column(nullable = false)
    private String date;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "todoitem_id")
    private TodoItem todoItem;

    public History() {
    }

    public History(String action, String date, String name) {
        this.action = action;
        this.date = date;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TodoItem getTodoItem() {
        return todoItem;
    }

    public void setTodoItem(TodoItem todoItem) {
        this.todoItem = todoItem;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", action='" + action + '\'' +
                ", date='" + date + '\'' +
                ", name='" + name + '\'' +
                ", todoItem=" + todoItem +
                '}';
    }
}
