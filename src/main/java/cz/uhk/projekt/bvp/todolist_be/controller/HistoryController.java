package cz.uhk.projekt.bvp.todolist_be.controller;

import cz.uhk.projekt.bvp.todolist_be.controller.path.UrlConstant;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history.CreateHistoryDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history.DeleteHistoryDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.CreateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.DeleteItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.HistoryDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.entity.History;
import cz.uhk.projekt.bvp.todolist_be.model.entity.TodoItem;
import cz.uhk.projekt.bvp.todolist_be.repository.HistoryInt;
import cz.uhk.projekt.bvp.todolist_be.validation.HistoryValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(UrlConstant.HISTORY)
public class HistoryController {

    @Autowired
    HistoryInt historyInt;
    @Autowired
    HistoryValidation historyValidation;
    private HistoryDTOOut historyDTOOut = new HistoryDTOOut();

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<History> getAllItems() {
        return historyInt.getAllHistory();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public HistoryDTOOut createItem(@RequestBody CreateHistoryDTOIn createHistoryDTOIn) {

        historyDTOOut = historyValidation.validateName(createHistoryDTOIn.getName());
        if (historyDTOOut.getStatusCode() == 1) {
            historyDTOOut = historyValidation.validateAction(createHistoryDTOIn.getAction());
            if (historyDTOOut.getStatusCode() == 1) {
                historyDTOOut = historyValidation.validateDate(createHistoryDTOIn.getDate());
                if (historyDTOOut.getStatusCode() == 1) {
                    historyDTOOut = historyInt.createHistory(createHistoryDTOIn);
                }
            }
        }
        return historyDTOOut;
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public HistoryDTOOut deleteItem(@RequestBody DeleteHistoryDTOIn deleteHistoryDTOIn) {
        return historyInt.deleteHistory(deleteHistoryDTOIn);
    }
}
