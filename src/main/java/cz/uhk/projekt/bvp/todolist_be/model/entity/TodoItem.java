package cz.uhk.projekt.bvp.todolist_be.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

import javax.persistence.*;

@Entity
public class TodoItem {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String name;

    private String description;

    @Column(nullable = false)
    private String date;

    @Column(nullable = false)
    private Boolean done;

    @Column(nullable = false)
    private Boolean readydelete;

    @OneToMany(mappedBy = "todoItem", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<History> history;

    public TodoItem() {
    }

    public TodoItem(String date, String description, String name, boolean done) {
        this.date = date;
        this.description = description;
        this.name = name;
        this.done = done;
        this.readydelete = false;
    }

    public Boolean getReadydelete() {
        return readydelete;
    }

    public void setReadydelete(Boolean readyDelete) {
        this.readydelete = readyDelete;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }


    @Override
    public String toString() {
        return "TodoItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", done=" + done +
                ", readydelete=" + readydelete +
                ", history=" + history +
                '}';
    }
}
