package cz.uhk.projekt.bvp.todolist_be.repository;

import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history.CreateHistoryDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history.DeleteHistoryDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.CreateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.DeleteItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.UpdateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.HistoryDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.entity.History;
import cz.uhk.projekt.bvp.todolist_be.model.entity.TodoItem;

import java.util.List;

public interface HistoryInt {

    HistoryDTOOut deleteHistory(DeleteHistoryDTOIn deleteHistoryDTOIn);

    List<History> getAllHistory();

    HistoryDTOOut createHistory(CreateHistoryDTOIn createHistoryDTOIn);
}
