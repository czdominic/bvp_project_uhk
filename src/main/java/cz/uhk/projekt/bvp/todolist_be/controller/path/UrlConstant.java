package cz.uhk.projekt.bvp.todolist_be.controller.path;


public class UrlConstant {
    public static final String WISHITEM = "/todoItem";
    public static final String HISTORY = "/history";
}
