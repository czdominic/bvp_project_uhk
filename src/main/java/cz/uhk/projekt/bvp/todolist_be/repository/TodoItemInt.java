package cz.uhk.projekt.bvp.todolist_be.repository;

import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.CreateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.DeleteItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem.UpdateItemDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.TodoItemDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.entity.TodoItem;
import java.util.List;

public interface TodoItemInt {


    TodoItemDTOOut deleteItem(DeleteItemDTOIn deleteItemDTOIn);

    TodoItemDTOOut updateItem(UpdateItemDTOIn updateItemDTOin);

    List<TodoItem> getAllItems();

    TodoItemDTOOut createItem(CreateItemDTOIn createItemDTOIn);
}
