package cz.uhk.projekt.bvp.todolist_be.model.dtoOut;

public class HistoryDTOOut {
    private int statusCode;
    private String message;


    public HistoryDTOOut() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
