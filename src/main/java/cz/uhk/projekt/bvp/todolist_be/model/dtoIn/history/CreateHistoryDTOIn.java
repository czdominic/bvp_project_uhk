package cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history;

import javax.persistence.Column;

public class CreateHistoryDTOIn {
    private int id;

    private String action;

    private String date;

    private String name;

    public String getAction() {
        return action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
