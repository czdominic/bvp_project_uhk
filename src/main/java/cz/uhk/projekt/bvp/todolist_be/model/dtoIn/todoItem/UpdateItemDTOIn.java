package cz.uhk.projekt.bvp.todolist_be.model.dtoIn.todoItem;

public class UpdateItemDTOIn {

    private int id;

    private Boolean done;

    public Boolean getDone() {
        return done;
    }

    public int getId() {
        return id;
    }
}
