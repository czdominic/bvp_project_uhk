package cz.uhk.projekt.bvp.todolist_be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
public class TodolistBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodolistBeApplication.class, args);
	}

}
