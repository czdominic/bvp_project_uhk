package cz.uhk.projekt.bvp.todolist_be.repository;

import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history.CreateHistoryDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoIn.history.DeleteHistoryDTOIn;
import cz.uhk.projekt.bvp.todolist_be.model.dtoOut.HistoryDTOOut;
import cz.uhk.projekt.bvp.todolist_be.model.entity.History;
import cz.uhk.projekt.bvp.todolist_be.model.entity.TodoItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HistoryDAO implements HistoryInt {


    private HistoryDTOOut historyDTOOut = new HistoryDTOOut();

    @PersistenceContext
    EntityManager em;

    @Override
    public HistoryDTOOut deleteHistory(DeleteHistoryDTOIn deleteHistoryDTOIn) {
        try {
            History historyToDelete = em.find(History.class, deleteHistoryDTOIn.getId());

            if (historyToDelete != null) {
                if (alsoDeleteItem(historyToDelete.getTodoItem().getId())) {
                    em.remove(em.find(TodoItem.class, historyToDelete.getTodoItem().getId()));
                } else {
                    em.remove(historyToDelete);
                }
                historyDTOOut.setStatusCode(1);
                historyDTOOut.setMessage("Historie smazána.");
            } else {
                historyDTOOut.setStatusCode(-1);
                historyDTOOut.setMessage("Historie nenalezena.");
            }
        } catch (
                Exception e) {
            historyDTOOut.setStatusCode(-1);
            historyDTOOut.setMessage("Chyba serveru, opakujte akci později.");
        }
        return historyDTOOut;
    }

    @Override
    public List<History> getAllHistory() {
        String query = "select h from History h";
        return em.createQuery(query).getResultList();
    }

    @Override
    public HistoryDTOOut createHistory(CreateHistoryDTOIn createHistoryDTOIn) {
        History history = new History(createHistoryDTOIn.getAction(), createHistoryDTOIn.getDate(), createHistoryDTOIn.getName());
        TodoItem todoItem = (em.find(TodoItem.class, createHistoryDTOIn.getId()));
        history.setTodoItem(todoItem);
        if (todoItem != null) {
            try {
                em.persist(history);
                historyDTOOut.setStatusCode(1);
                historyDTOOut.setMessage("Historie vytvořena.");
            } catch (Exception e) {
                historyDTOOut.setStatusCode(-1);
                historyDTOOut.setMessage("Chyba serveru.");
            }
        } else {
            historyDTOOut.setStatusCode(-1);
            historyDTOOut.setMessage("Nebyl nalezen odpovídající item.");
        }
        return historyDTOOut;
    }


    private boolean alsoDeleteItem(int itemId) {
        Query query = em.createNativeQuery("SELECT count(*) FROM history h inner join todo_item t on t.id=h.todoitem_id where t.id=:id and t.readydelete=true");
        query.setParameter("id", itemId);
        int count = ((Number) query.getSingleResult()).intValue();
        return count == 1;
    }

}
